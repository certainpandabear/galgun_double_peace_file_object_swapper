# -*- coding: utf-8 -*-
"""The actual code logic to swap the files."""

import argparse
from pprint import pprint

from os.path import isfile, isdir, split, join

__author__ = "Ruudi"


def _check_group_chunk(file: object, byte_count: int, hex_comparison: str):
    """A helper function to help check if the next byte_count amount of bytes matches with
    with the given expected hexes.

    This should be run only during file object context (with open('filename.txt') as file).

    :param file: The file opened under context.
    :param byte_count: How many bytes do we want to read up.
    :param hex_comparison: String to compare with what was read. 
    :return: 
    """
    group_chunks = file.read(byte_count)
    if hex_comparison == "".join(["{:02X}".format(c) for c in group_chunks]):
        return True
    else:
        # Moving the pointer back to where it was.
        file.seek(-byte_count, 1)
    return False


def _get_data_from_cooked_uexp(file: object):
    """A helper function to help us get the necessary data from the cooked .uexp file.

    We're looking for the magic bytes of "FF FF CD" as the data afterwards is what we're after.
    
    This should be run only during file object context (with open('filename.txt') as file).

    :param file: File object of the cooked .uexp.
    :return: The tail of the data for modified .uexp.
    """
    magic_bytes = 'FF FF CD'.replace(' ', '')
    while True:
        chunk = file.read(1)
        if "{:02X}".format(chunk[0]) == magic_bytes[:2]:
            # First byte discovered, now to check the next 2 bytes
            if _check_group_chunk(file=file, byte_count=2, hex_comparison=magic_bytes[2:]):
                file.seek(2, 1)
                return file.read()
        if len(chunk) == 0:
            raise RuntimeError('Could not find the magic bytes in file [%s]' % magic_bytes)


def _get_data_from_uexp(file: object):
    """A helper function to help us get the necessary data from the .uexp file.

    We're looking for the magic bytes of "FF FF CD" as the data before this is what we're after.

    This should be run only during file object context (with open('filename.txt') as file).

    :param file: File object of the .uexp.
    :return: The head of the data for modified .uexp.
    """
    magic_bytes = 'FF FF CD'.replace(' ', '')
    while True:
        chunk = file.read(1)
        if "{:02X}".format(chunk[0]) == magic_bytes[:2]:
            # First byte discovered, now to check the next 2 bytes
            if _check_group_chunk(file=file, byte_count=2, hex_comparison=magic_bytes[2:]):
                file.seek(2, 1)
                current_pointer = file.tell()
                file.seek(0)
                return file.read(current_pointer)
        if len(chunk) == 0:
            raise RuntimeError('Could not find the magic bytes in file [%s]' % magic_bytes)


def _get_file_size_information(file: object, ignore_last_bytes: int = 4):
    """A helper function to return the file size information in tuple.
    
    This should be run only during file object context (with open('filename.txt') as file).
    
    :param file: The file opened under context.
    :param ignore_last_bytes: How many bytes at the end we'll ignore. For Gal*Gun 2, last 4 are often ignored. 
    :return: Tuple with following information file size in int, file size in hex, file size hex reversed.
    """
    current_position = file.tell()
    file.seek(-ignore_last_bytes, 2)  # Move pointer to the end sans the last bytes.
    size = file.tell()  # Pick the file sizes.
    file.seek(current_position, 0)  # Reset the pointer back to where it originally was.
    size_in_hex = "{:02X}".format(size)
    if len(size_in_hex) % 2:
        # Making sure hex length is even
        size_in_hex = '0' + size_in_hex
    return size, size_in_hex, _reverse_bytes_hex(size_in_hex)


def _get_file_size_location(file: object, target_hex: str):
    """Helper function to return the file pointer location where the .uasset holds the .uexp object's
    size data.
    
    This should be run only during file object context (with open('filename.txt') as file).
    
    :param file: The file opened under context.
    :param target_hex: The string hex pattern that must be found.
        The hex size presentation of .uexp must be reversed for this one.
    :return: The file pointer location in int.
    """
    while True:
        chunk = file.read(1)
        if "{:02X}".format(chunk[0]) == target_hex[:2]:
            # First byte discovered, now to check the next 2 bytes
            if _check_group_chunk(file=file, byte_count=2, hex_comparison=target_hex[2:]):
                file.seek(-1, 1)
                return file.tell()
        if len(chunk) == 0:
            raise RuntimeError('Could not find the target hex in file [%s]' % target_hex)


def _output_changes(output_uexp: str,
                    uexp_data: bytes,
                    cooked_uexp_data: bytes,
                    output_uasset: str,
                    uasset_data: bytes,
                    uasset_location: int):
    """Helper function to merge the head data of non-cooked .uexp and tail data of cooked .uexp and
    output the changes to pre-selected path. Additionally, will output the new .uasset to support the changes
    that the modified combined .uexp has.
    
    :param output_uexp: Path where the new .uexp data will be combined and output to.
    :param uexp_data: The head data that will be written first to combined .uexp.
    :param cooked_uexp_data: The tail data that will be written after to the combined .uexp.
    :param output_uasset: Path where the new .uasset data will be output to.
    :param uasset_data: .uasset data as a whole.
    :param uasset_location: The file pointer location where the .uasset holds the .uexp size data.
    :return: 
    """
    with open(output_uexp, 'r+b') as output_exp_file:
        output_exp_file.write(uexp_data)
        output_exp_file.write(cooked_uexp_data)
        output_exp_file.truncate()
        uexp_size, uexp_hex_size, uexp_hex_size_rev = _get_file_size_information(output_exp_file)
        _print_uexp_file_size(title='output .uexp',
                              uexp_size=uexp_size,
                              uexp_hex_size=uexp_hex_size,
                              uexp_hex_size_rev=uexp_hex_size_rev)

    with open(output_uasset, 'r+b') as output_uasset_file:
        output_uasset_file.write(uasset_data)
        output_uasset_file.seek(uasset_location)
        for hex in [uexp_hex_size[i:i + 2] for i in range(0, len(uexp_hex_size), 2)]:
            output_uasset_file.write(bytes.fromhex(hex))


def _reverse_bytes_hex(hex_val: str):
    """Will reverse the given bytes hex string.
    :param hex_val: String presentation of the bytes hex.
    :return: Reversed string presentation of the bytes hex, with spaces removed.
    """
    return ''.join(reversed([hex_val[i:i + 2] for i in range(0, len(hex_val), 2)])).replace(' ', '')


def _print_uexp_file_size(title: str, uexp_size: int, uexp_hex_size: str, uexp_hex_size_rev: str):
    pprint('%s file [%s]:' % (title, uexp))
    pprint('  Size: {:>17}'.format(uexp_size))
    pprint(
        '  In hex: {:>17}'.format(' '.join([uexp_hex_size[i:i + 2] for i in range(0, len(uexp_hex_size), 2)])))
    pprint('  In hex reversed: %s' % ' '.join(
        [uexp_hex_size_rev[i:i + 2] for i in range(0, len(uexp_hex_size_rev), 2)]))


def run_job(uasset: str, uexp: str, uexp_cooked: str, location: str):
    if isfile(uasset) and isfile(uexp) and isfile(uexp_cooked) and isdir(location):
        output_uasset = join(location, split(uasset)[1])
        output_uexp = join(location, split(uexp)[1])

        with open(uexp, 'rb') as uexp_file:
            uexp_size, uexp_hex_size, uexp_hex_size_rev = _get_file_size_information(uexp_file)
            _print_uexp_file_size(title='.uexp',
                                  uexp_size=uexp_size,
                                  uexp_hex_size=uexp_hex_size,
                                  uexp_hex_size_rev=uexp_hex_size_rev)
            uexp_data = _get_data_from_uexp(uexp_file)

        with open(uasset, 'rb') as uasset_file:
            uasset_location = _get_file_size_location(uasset_file, uexp_hex_size_rev)
            uasset_file.seek(0)
            uasset_data = uasset_file.read()

        with open(uexp_cooked, 'rb') as uexp_cooked_file:
            cooked_uexp_data = _get_data_from_cooked_uexp(uexp_cooked_file)

        _output_changes(output_uexp=output_uexp,
                        output_uasset=output_uasset,
                        uexp_data=uexp_data,
                        cooked_uexp_data=cooked_uexp_data,
                        uasset_data=uasset_data,
                        uasset_location=uasset_location)

    else:
        pprint('Provided arguments were invalid')
        pprint('uasset as file:      %s' % isfile(uasset))
        pprint('uexp as file:        %s' % isfile(uexp))
        pprint('uexp_cooked as file: %s' % isfile(uexp_cooked))
        pprint('location as dir:     %s' % isdir(location))
    return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Swap object data for Gal*Gun 2')
    parser.add_argument('uasset', nargs='?', help='.uasset file that will be modified')
    parser.add_argument('uexp', nargs='?', help='.uexp file that will be modified')
    parser.add_argument('uexp_cooked', nargs='?', help='Cooked .uexp file that will be used to alter .uasset and .uexp')
    parser.add_argument('location',
                        nargs='?',
                        help=(
                            'Location where the modified .uasset and .uexp will be output to using '
                            'the same provided name. '
                            'THIS WILL OVERWRITE EXISTING FILES!'))
    args = parser.parse_args()

    uasset = args.uasset
    uexp = args.uexp
    uexp_cooked = args.uexp_cooked
    location = args.location

    run_job(uasset=uasset, uexp=uexp, uexp_cooked=uexp_cooked, location=location)
