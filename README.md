# Gal*Gun Double Peace File Swapper

Inspired by Darkhawk02's [video](https://youtu.be/Piq4DStXXo4) on modding Gal*Gun: Double Peace.

## Using the ggdp file object data swapper script

1. Install python3.6

2. Clone this repository

3. Go to this repository and start a python virtual environment

    3.1 Within the project: `python -m venv env` if you haven't created one yet.
    
    3.2 Within the project: `env\Scripts\activate` and you should get a nice (env) at the start of your command prompt.

4. Run the script with the following command: `python ggdp_fs SOURCE_FILE_PATH TARGET_FILE_PATH OFFSET --output OUTPUT_FILE_PATH --data DATA_TYPE`

    4.1 The `OFFSET` is to be given by the target object's table address in 4 bytes with or without the `0x` prefix.
    For example, target address is at `0x0000061E`, you type the `OFFSET` as `0x0000061E` or `0000061E`.

    4.2 The `--data` is optional and will default to treating the object data as `sound`.
    Valid values are `sound`, `texture`

5. Running the script as batch task with the following command: `python ggdp_fs --batch batch_file.txt --folder FOLDER`

    5.1 The batch_file.txt is comma-separated file that has runs each line as single script instance execution.
    
    5.2 In the batch file, ALL arguments must be provided.
    
    5.3 Batch file arguments are in this order for one line: `SOURCE_FILE_NAME, TARGET_FILE_NAME, OFFSET, OUTPUT_FILE, DATA_TYPE`
    
    5.4 Files must be found within `FOLDER` as the batch will run inside the folder and outputs into the folder.

# Gal*Gun 2 File Swapper

Inspired by Darkhawk02's [video](https://youtu.be/WlIdhSv5HyQ) on modding Gal*Gun 2.

## Using the gg2 file object data swapper script

1. Install python3.6

2. Clone this repository

3. Go to this repository and start a python virtual environment

    3.1 Within the project: `python -m venv env` if you haven't created one yet.
    
    3.2 Within the project: `env\Scripts\activate` and you should get a nice (env) at the start of your command prompt.

4. Run the script with the following command: `python gg2_fs UASSET_FILE_PATH UEXP_FILE_PATH COOKED_UEXP_FILE_PATH OUTPUT_LOCATION_DIR`

    4.1 `python gg2_fs --help` for more information.
    
    4.2 Providing `OUTPUT_LOCATION_DIR` with `.` means it'll write to current folder the script is running.

### FAQ

#### Why not separate GG:DP and GG2 FS scripts to separate repository?
 
I'm lazy \o/

#### Any notable works done with these scripts?

GalGun Double Peace File Swapper has been used for these following projects in the order they were released:

1. [Compa Mod](https://www.youtube.com/watch?v=DIzNcgONE3E)
2. [Summer Fun Mod](https://www.youtube.com/watch?v=6f46jZs-GOw)
3. [Purple Heart Mod](https://www.youtube.com/watch?v=Ei2EwIrdy0E)
4. [Noire Mod](https://www.youtube.com/watch?v=DUUhllMYWoI)
5. [NepGear Mod](https://www.youtube.com/watch?v=_7cCLZK3IKA)