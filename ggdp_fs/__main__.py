# -*- coding: utf-8 -*-
"""The actual code logic to swap the files."""

import argparse
from pprint import pprint

from os.path import isfile, isdir, join

import sys

__author__ = "Ruudi"


def figure_out_object_size_from_file(target, offset):
    """To figure out the size of the data object based on the given offset.
    :param target: The file that is being targeted. 
    :param offset: Offset given for the object data. The offset will be reversed.
    :return: Tuple of the object size in 4 bytes hex string representation,
                file location in int and given offset location in int.
    """
    reversed_offset = _reverse_4bytes_hex(offset)
    first_byte = reversed_offset[0:2]
    object_size = None
    size_location = None
    offset_location = None
    with open(target, 'rb') as file:
        while True:
            chunk = file.read(1)
            if len(chunk) == 0:
                break
            hex_val = "{:02X}".format(chunk[0])
            if first_byte == hex_val:
                discovered_location = file.tell()
                if _check_group_chunk(file_object=file, byte_count=3, hex_comparison=reversed_offset[2:]):
                    offset_location = discovered_location - 1
                    file.seek(-8, 1)
                    size_location = file.tell()
                    memory_size = file.read(4)
                    object_size = " ".join(["{:02X}".format(c) for c in memory_size])
                    break
    return object_size, size_location, offset_location


def duplicate_object_to_end_of_file(target, output, offset, size):
    """Will create a new file that has the target file
    and duplicate of the object data.
    :param target: The file that is being targeted. 
    :param output: The file that will be written to.
    :param offset: Offset given for the object data.
    :param size: Size that was discovered for the given offset.
        This will be reversed to calculate the object data size.
    :return: Duplicated object's offset in where it begins.
    """
    reversed_size = _reverse_4bytes_hex(size)
    calculated_offset = int(offset, 16)
    calculated_size = int(reversed_size, 16)
    discovered_offset = None
    with open(output, 'wb') as in_file:
        # Copy the file.
        with open(target, 'rb') as out_file:
            while True:
                chunk = out_file.read(16)
                if len(chunk) == 0:
                    break
                in_file.write(chunk)

        # Adding Darkhawk02's FF padding.
        for i in range(4):
            in_file.write(bytes.fromhex('FF'))
        discovered_offset = "{:08X}".format(in_file.tell())
        # Copying the object to the end of the file.
        with open(target, 'rb') as out_file:
            out_file.seek(calculated_offset)
            in_file.write(out_file.read(calculated_size))
    return discovered_offset


def update_file_offset(duplicate_file, new_offset, offset_location):
    """Update the given file's offset data with new one.

    :param duplicate_file: The file that we're handling. 
    :param new_offset: The new offset information in 4 bytes. This will be reversed.
    :param offset_location: The exact location of the file where the offset information is defined.
                            Given offset_location will be reversed for input purposes.
    :return: None
    """
    reversed_offset = _reverse_4bytes_hex(new_offset)
    with open(duplicate_file, 'r+b') as file:
        file.seek(offset_location)
        file.write(bytes.fromhex(reversed_offset[0:2]))
        file.write(bytes.fromhex(reversed_offset[2:4]))
        file.write(bytes.fromhex(reversed_offset[4:6]))
        file.write(bytes.fromhex(reversed_offset[6:8]))
    return None


def replace_target_file_object_data(source_file, target_file, duplicate_obj_offset, data_type='sound'):
    """Modify the target file with given source file.

    We're looking for magic bytes of "18 00 00 00 00 00 00 00 14 00 00 00" to locate a metadata location
    before timestamp and using that to our knowledge, we'll be overwriting the target file's information.

    The actual object data begins 40 bytes after the magic bytes for sound data, 88 bytes for texture data.

    :param source_file: The file which has the content we'd want to add to the target file.
    :param target_file: The file which will be on the receiving end of modification.
                        NOTE: SHOULD BE THE DUPLICATED FILE AS TO NOT OVERWRITE THE ORIGINAL UNALTERED FILE.
    :param duplicate_obj_offset: Offset for the given duplicated object data. The search will start there.
    :param data_type: Determines what data type are we handling. 
    :return: None
    """
    magic_bytes = '18 00 00 00 00 00 00 00 14 00 00 00'.replace(' ', '')
    data_type_byte_adjustment = {'sound': 40, 'texture': 88}
    pprint('Replacing target file content with data type [%s]...' % data_type)
    with open(target_file, 'r+b') as in_file:
        in_file.seek(int(duplicate_obj_offset, 16))
        while True:
            chunk = in_file.read(1)
            if "{:02X}".format(chunk[0]) == '18':
                # First byte discovered, now to check the next 11 bytes
                if _check_group_chunk(file_object=in_file, byte_count=11, hex_comparison=magic_bytes[2:]):
                    in_file.seek(data_type_byte_adjustment.get(data_type, 88), 1)
                    break

            if len(chunk) == 0:
                raise RuntimeError("Couldn't locate specific metadata for target file [%s]" % target_file)

        with open(source_file, 'rb') as out_file:
            while True:
                chunk = out_file.read(1)
                if "{:02X}".format(chunk[0]) == '18':
                    # First byte discovered, now to check the next 11 bytes
                    if _check_group_chunk(file_object=out_file, byte_count=11, hex_comparison=magic_bytes[2:]):
                        out_file.seek(data_type_byte_adjustment.get(data_type, 88), 1)
                        break
                if len(chunk) == 0:
                    raise RuntimeError("Couldn't locate specific metadata for source file [%s]" % source_file)
            out_file_start = out_file.tell()
            out_file_end = out_file.seek(0, 2)
            out_file.seek(out_file_start)
            while True:
                # Now we're doing the actual modification.
                chunk = out_file.read(1)
                if len(chunk) == 0:
                    break
                in_file.write(chunk)
                sys.stdout.write("\r {progress} / 100 %                          ".format(
                    progress=round((out_file.tell() - out_file_start) * 100 / (out_file_end - out_file_start), 2)))
                sys.stdout.flush()
            print('')
        # Everything after the current pointer will be discarded.
        in_file.truncate()
    pprint('Target file object data replacement done')


def update_modified_object_data_size_info(target_file, duplicate_obj_offset, size_location):
    """Will determine how many bytes was used for the modified object data and then
    update the original size location information with newly figured out object data size.

    :param target_file: File that will be updated with.
    :param duplicate_obj_offset: Offset given in hex string presentation. 
    :param size_location: The location in the file where the size information should be updated with.
    """
    calculated_offset_location = int(duplicate_obj_offset, 16)
    with open(target_file, 'r+b') as file:
        end_file = file.seek(0, 2)
        calculated_size = end_file - calculated_offset_location
        reversed_size_hex = _reverse_4bytes_hex("{:08X}".format(calculated_size))
        file.seek(size_location)
        file.write(bytes.fromhex(reversed_size_hex[0:2]))
        file.write(bytes.fromhex(reversed_size_hex[2:4]))
        file.write(bytes.fromhex(reversed_size_hex[4:6]))
        file.write(bytes.fromhex(reversed_size_hex[6:8]))


def clean_up(target_file, data_type):
    """Clean up process after the source and target files have been combined.
    
    :param target_file: File that will be given the clean up treatment.
    :param data_type: What data type was the object.
    """
    if data_type == 'texture':
        pprint('Removing last 4 bytes due to [%s] data type' % data_type)
        with open(target_file, 'r+b') as file:
            file.seek(-4, 2)
            file.truncate()


def _check_group_chunk(file_object, byte_count, hex_comparison):
    """A helper function to help check if the next byte_count amount of bytes matches with
    with the given expected hexes.
    
    This should be run only during file object context (with open('filename.txt') as file).
    
    :param file_object: The file opened under context.
    :param byte_count: How many bytes do we want to read up.
    :param hex_comparison: String to compare with what was read. 
    :return: 
    """
    group_chunks = file_object.read(byte_count)
    if hex_comparison == "".join(["{:02X}".format(c) for c in group_chunks]):
        return True
    else:
        # Moving the pointer back to where it was.
        file_object.seek(-byte_count, 1)
    return False


def _reverse_4bytes_hex(hex_val):
    """Will reverse the given 4 bytes hex string.
    :param hex_val: String presentation of the 4 bytes hex.
    :return: Reversed string presentation of the 4 bytes hex, with spaces removed.
    """
    return ''.join(reversed([hex_val[i:i + 2] for i in range(0, len(hex_val), 2)])).replace(' ', '')


def hex_dump(target):
    """To simply hex dump the file for sake of curiosity."""
    offset = 0
    with open(target, 'rb') as file:
        while True:
            chunk = file.read(16)
            if len(chunk) == 0:
                break

            text = ''.join([chr(i) for i in chunk])
            output = "{:#08X}".format(offset) + ": "
            output += " ".join("{:02X}".format(c) for c in chunk[:8])
            output += " | "
            output += " ".join("{:02X}".format(c) for c in chunk[8:])

            if len(chunk) != 16:
                output += "   " * (16 - len(chunk)) + text
            else:
                output += " " + text
            offset += 16
            print(output)


def run_job(source: str, target: str, offset: str, output: str, data: str):
    duplicate_obj_offset = None
    if not data or data not in ('sound', 'texture'):
        data_type = 'sound'
    else:
        data_type = data
    if target and isfile(target) and source and isfile(source):
        object_size, size_location, offset_location = figure_out_object_size_from_file(target=target,
                                                                                       offset=offset)
        if object_size and size_location:
            pprint('Target object size: [%s]' % object_size)
            pprint('Target size file location: [%s]' % size_location)
            pprint('Given offset for target file location: [%s]' % offset_location)
        else:
            pprint('Unable to locate object from given offset')
            return None
        if output and object_size and size_location:
            duplicate_obj_offset = duplicate_object_to_end_of_file(target=target,
                                                                   offset=offset,
                                                                   size=object_size,
                                                                   output=output)
        if duplicate_obj_offset:
            pprint("Duplicated object's offset [%s]" % duplicate_obj_offset)
        if duplicate_obj_offset and output:
            update_file_offset(duplicate_file=output,
                               new_offset=duplicate_obj_offset,
                               offset_location=offset_location)
            replace_target_file_object_data(source_file=source,
                                            target_file=output,
                                            duplicate_obj_offset=duplicate_obj_offset,
                                            data_type=data_type)
            clean_up(target_file=output, data_type=data_type)
            update_modified_object_data_size_info(target_file=output,
                                                  duplicate_obj_offset=duplicate_obj_offset,
                                                  size_location=size_location)
    else:
        pprint('Could not locate both source [%s] and target [%s] files' % (source, target))
    return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Swap object data for Gal*Gun: Double Peace')
    parser.add_argument('source', nargs='?', help='Source file that will be used to alter with')
    parser.add_argument('target', nargs='?', help='Target file that will be used as basis for modification')
    parser.add_argument('offset', nargs='?', help='The offset location in reverse byte order')
    parser.add_argument('--output', help='File to output to')
    parser.add_argument('--data', help='What sort of data object are handling (sound, texture)')
    parser.add_argument('--batch',
                        help='Use specific file for batch execution of the script. Must be used together with --folder')
    parser.add_argument('--folder',
                        help='The folder where the batch job will be executed. Must be used together with --batch')
    args = parser.parse_args()
    if args.folder and isdir(args.folder) and args.batch and isfile(args.batch):
        counter = 0
        with open(args.batch, 'r') as batch_file:
            while True:
                stream = batch_file.readline()
                if len(stream) == 0:
                    break
                counter += 1
                line = stream.split(',')
                source = line[0].strip()
                target = line[1].strip()
                offset = line[2].strip()
                if '0x' in offset[0:2]:
                    offset = offset[2:]
                output = line[3].strip()
                data = line[4].strip()
                pprint('==== Batch job #%s BEGIN ====' % counter)
                pprint('Source file: [%s]' % source)
                pprint('Target file: [%s]' % target)
                pprint('Offset: [%s]' % offset)
                pprint('Output file: [%s]' % output)
                pprint('Data type: [%s]' % data)
                run_job(source=join(args.folder, source),
                        target=join(args.folder, target),
                        offset=offset,
                        output=join(args.folder, output),
                        data=data)
                pprint('==== Batch job #[%s] END ====' % counter)
        pprint('Batch job done')
    else:
        source = args.source
        target = args.target
        offset = args.offset
        if '0x' in offset[0:2]:
            offset = offset[2:]
        output = args.output
        data = args.data
        run_job(source=source, target=target, offset=offset, output=output, data=data)
